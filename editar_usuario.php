<?php

session_start();

require_once __DIR__ . '/persistencia/usuarios.php';

if (!isset($_SESSION['usuario_logado'])
        || $_SESSION['usuario_logado']['codigo'] != $_REQUEST['codigo']) {
    die('Acesso negado');
}

/*
if (!(isset($_SESSION['usuario_logado'])
        && $_SESSION['usuario_logado']['codigo'] == $_REQUEST['codigo'])) {
    die('Acesso negado');
}
*/

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = [
        'codigo' => $_REQUEST['codigo'],
        'nome' => $_REQUEST['nome'],
        'cidade' => $_REQUEST['cidade'],
    ];
    
    salvar_usuario($usuario);
    
    header('Location: index.php');
} else {
    $codigo = $_REQUEST['codigo'];
    $usuario = buscar_usuario($codigo);
}

require_once __DIR__ . '/interface/editar_usuario.php';
