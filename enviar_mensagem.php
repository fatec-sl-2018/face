<?php

require_once __DIR__ . '/persistencia/mensagens.php';

$mensagem = [
    'texto' => $_REQUEST['texto'],
    'codigo_usuario' => $_REQUEST['codigo'],
];

incluir_mensagem($mensagem);

$mensagens = listar_mensagens($_REQUEST['codigo']);

?>
<?php foreach ($mensagens as $m) { ?>
    <p><?= $m['texto'] ?></p>
<?php } ?>
