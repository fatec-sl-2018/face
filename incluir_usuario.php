<?php

session_start();

require_once __DIR__ . '/persistencia/usuarios.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = [
        'nome' => $_REQUEST['nome'],
        'cidade' => $_REQUEST['cidade'],
    ];
    
    incluir_usuario($usuario);
    
    header('Location: index.php');
}

require_once __DIR__ . '/interface/incluir_usuario.php';
