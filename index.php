<?php

session_start();

require_once __DIR__ . '/persistencia/usuarios.php';

if (empty($_REQUEST['nome'])) {
    $usuarios = listar_usuarios();
} else {
    $usuarios = pesquisar_usuarios($_REQUEST['nome']);
}

require_once __DIR__ . '/interface/index.php';
