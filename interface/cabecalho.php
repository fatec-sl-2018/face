<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Face</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
          <a class="navbar-brand" href="index.php">Face</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Início <span class="sr-only">(current)</span></a>
              </li>
              
            <?php if (empty($_SESSION['usuario_logado'])) { ?>
              <li class="nav-item">
                  <a class="nav-link" href="login.php">Faça seu login</a>
              </li>
            <?php } else { ?>
              <li class="nav-item">
                <a class="nav-link" href="perfil.php?codigo=<?= $_SESSION['usuario_logado']['codigo'] ?>">
                    <?= $_SESSION['usuario_logado']['nome'] ?>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="logout.php">Logout</a>
              </li>
            <?php } ?>
            </ul>
            <form class="form-inline my-2 my-md-0" action="index.php">
              <input class="form-control" type="text" placeholder="Pesquisar usuários"
                     name="nome">
            </form>
          </div>
        </nav>

        <div class="container">
        