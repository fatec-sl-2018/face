<?php require_once __DIR__ . '/cabecalho.php'; ?>
        
<h2>Editando o usuário <?= $usuario['nome'] ?></h2>

<form method="POST" action="editar_usuario.php?codigo=<?= $usuario['codigo'] ?>">
    <table>
        <tr>
            <th>Nome</th>
            <td>
                <input name="nome"
                       value="<?= $usuario['nome'] ?>"
                       />
            </th>
        </tr>
        <tr>
            <th>Cidade</th>
            <td>
                <input name="cidade"
                       value="<?= $usuario['cidade'] ?>"
                       />
            </th>
        </tr>
    </table>

    <div>
        <input type="submit" value="Salvar"/>
    </div>
</form>

<?php require_once __DIR__ . '/rodape.php'; ?>
