<?php require_once __DIR__ . '/cabecalho.php'; ?>

<?php if (empty($_SESSION['usuario_logado'])) { ?>
<p><a href="login.php">Faça seu login</a></p>
<?php } else { ?>
<p>Bem vindo(a), <?= $_SESSION['usuario_logado']['nome'] ?></p>
<p><a href="logout.php">Logout</a></p>
<?php } ?>

<h2>Lista de usuários</h2>

<form action="index.php">
    <input name="nome"/>
    <input type="submit" value="Pesquisar"/>
</form>

<ul>
    <?php foreach ($usuarios as $u): ?>
        <li>
            <a href="perfil.php?codigo=<?= $u['codigo'] ?>"><?= $u['nome'] ?></a>
        </li>
    <?php endforeach; ?>
</ul>

<?php require_once __DIR__ . '/rodape.php'; ?>
