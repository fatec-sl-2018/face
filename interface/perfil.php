<?php require_once __DIR__ . '/cabecalho.php'; ?>

    <h2>Perfil do usuário <?= $usuario['nome'] ?></h2>

    <table>
        <tr>
            <th>Nome</th>
            <td><?= $usuario['nome'] ?></th>
        </tr>
        <tr>
            <th>Cidade</th>
            <td><?= $usuario['cidade'] ?></td>
        </tr>
    </table>

    <div>
        <?php if (isset($_SESSION['usuario_logado'])
                && $_SESSION['usuario_logado']['codigo'] == $usuario['codigo']) { ?>
        <a href="editar_usuario.php?codigo=<?= $usuario['codigo'] ?>">
            Editar
        </a>
        <?php } ?>
    </div>

    <form onsubmit="enviarMensagem(event)">
        <input name="texto" id="texto"/>
        <input type="submit" value="Enviar"/>
    </form>

    <div id="listaMensagens">
        <?php foreach ($mensagens as $m) { ?>
            <p><?= $m['texto'] ?></p>
        <?php } ?>
    </div>
    
    <script>
        
        function enviarMensagem(event) {
            event.preventDefault();
            
            var codigoUsuario = <?= $usuario['codigo'] ?>;

            // usando HTML retornado do servidor
            /*
            $.ajax({
                method: 'POST',
                url: 'enviar_mensagem.php',
                data: {
                    codigo: codigoUsuario,
                    texto: $('#texto').val()
                },
                success: function (resposta) {
                    $('#listaMensagens').html(resposta);
                    $('#texto').val('');
                }
            });
            */
           
            // usando JSON retornado do servidor
            $.ajax({
                method: 'POST',
                url: 'enviar_mensagem_json.php',
                data: {
                    codigo: codigoUsuario,
                    texto: $('#texto').val()
                },
                success: function (resposta) {
                    debugger;
                    
                    // TRABALHO 07
                    var novoHtml = '?';
                    
                    $('#listaMensagens').html(novoHtml);
                    $('#texto').val('');
                }
            });

        }
        
    </script>

<?php require_once __DIR__ . '/rodape.php'; ?>
