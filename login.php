<?php

session_start();

require_once __DIR__ . '/persistencia/usuarios.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = buscar_usuario_para_login(
        $_REQUEST['nome_usuario'],
        $_REQUEST['senha']
    );
    
    if ($usuario == null) {
        die('Usuário ou senha incorretos');
    } else {
        $_SESSION['usuario_logado'] = $usuario;
        
        header('Location: index.php');
        exit();
    }
}

require_once __DIR__ . '/interface/login.php';
