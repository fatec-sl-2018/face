<?php

session_start();

require_once __DIR__ . '/persistencia/usuarios.php';
require_once __DIR__ . '/persistencia/mensagens.php';

$codigo = $_REQUEST['codigo'];
$usuario = buscar_usuario($codigo);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $mensagem = [
        'texto' => $_REQUEST['texto'],
        'codigo_usuario' => $codigo,
    ];
    
    incluir_mensagem($mensagem);
}

$mensagens = listar_mensagens($codigo);

require_once __DIR__ . '/interface/perfil.php';
