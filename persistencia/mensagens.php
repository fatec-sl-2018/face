<?php

require_once __DIR__ . '/conexao.php';

function incluir_mensagem($mensagem) {
    global $conexao;
    
    //$comando = $conexao->prepare('insert into mensagens (texto, codigo_usuario) values (?, ?)');
    //$comando->execute([
    //    $mensagem['texto'],
    //    $mensagem['codigo_usuario'],
    //]);
    
//    $comando = $conexao->prepare('insert into mensagens (texto, codigo_usuario) values (:meu_texto, :meu_codigo)');
//    $comando->execute([
//        'meu_texto' => $mensagem['texto'],
//        'meu_codigo' => $mensagem['codigo_usuario'],
//    ]);
    
    $comando = $conexao->prepare('insert into mensagens (texto, codigo_usuario) values (:texto, :codigo_usuario)');
    $comando->execute($mensagem);
}

function listar_mensagens($codigo_usuario) {
    global $conexao;
    
    $comando = $conexao->prepare('select * from mensagens where codigo_usuario = ? order by codigo desc');
    $comando->execute([
        $codigo_usuario,
    ]);
    
    return $comando->fetchAll();
}
