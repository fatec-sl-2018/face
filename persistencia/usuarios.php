<?php

require_once __DIR__ . '/conexao.php';

function listar_usuarios() {
    global $conexao;
    return $conexao->query('select * from usuarios order by nome')->fetchAll();
}

function pesquisar_usuarios($filtro) {
    global $conexao;
    $filtro = trim($filtro);
    $filtro = strtolower($filtro);
    
    //return $conexao->query("select * from usuarios where lower(nome) like '%$filtro%' order by nome")->fetchAll();
    
    $comando = $conexao->prepare('select * from usuarios where lower(nome) like ? order by nome');
    $comando->execute(['%' . $filtro . '%']);
    return $comando->fetchAll();
}

function buscar_usuario($codigo) {
    global $conexao;
    
    // INSEGURO - SEM PARÂMETROS SQL
    //$comando = $conexao->query("select * from usuarios where codigo = $codigo");
    //$resultado = $comando->fetchAll();
    
    // SEGURO
    $comando = $conexao->prepare('select * from usuarios where codigo = ?');
    $comando->execute([$codigo]);
    $resultado = $comando->fetchAll();
    
    return $resultado[0];
}

function salvar_usuario($usuario) {
    global $conexao;
    
    // o comando abaixo vai executar algo assim:
    //
    // update usuarios set nome = 'diogo', cidade = 'rio preto' where codigo = 500
    
    $sql = "update usuarios"
            . " set nome = '" . $usuario['nome'] . "'"
            . " , cidade = '" . $usuario['cidade'] . "'"
            . " where codigo = " . $usuario['codigo'];
    
    //var_dump($sql);
    
    $conexao->query($sql);
}

function incluir_usuario($usuario) {
    global $conexao;
    
    // o comando abaixo vai executar algo assim:
    //
    // insert into usuarios (nome, cidade) values ('diogo', 'rio preto')
    
    $nome = $usuario['nome'];
    $cidade = $usuario['cidade'];
    $sql = "insert into usuarios (nome, cidade) values ('$nome', '$cidade')";
    
    //var_dump($sql);
    
    $conexao->query($sql);
}

function buscar_usuario_para_login($nome_usuario, $senha) {
    global $conexao;
    
    $comando = $conexao->prepare('select * from usuarios where nome_usuario = ? and senha = ?');
    $comando->execute([
        $nome_usuario,
        $senha
    ]);
    
    $lista_usuarios = $comando->fetchAll();
    
    if (empty($lista_usuarios)) {
        return null;
    } else {
        return $lista_usuarios[0];
    }
}
